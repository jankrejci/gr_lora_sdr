#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# SPDX-License-Identifier: GPL-3.0
#
# GNU Radio Python Flow Graph
# Title: LoRa Decode
# GNU Radio version: v3.8.2.0-57-gd71cd177

from gnuradio import gr
from gnuradio.filter import firdes
import sys
import signal
from argparse import ArgumentParser
from gnuradio.eng_arg import eng_float, intx
from gnuradio import eng_notation
from gnuradio import zeromq
import lora
import osmosdr
import time
import relative_path  # embedded python module
try:
    import configparser
except ImportError:
    import ConfigParser as configparser
try:
    import configparser
except ImportError:
    import ConfigParser as configparser
try:
    import configparser
except ImportError:
    import ConfigParser as configparser
try:
    import configparser
except ImportError:
    import ConfigParser as configparser
try:
    import configparser
except ImportError:
    import ConfigParser as configparser
try:
    import configparser
except ImportError:
    import ConfigParser as configparser
try:
    import configparser
except ImportError:
    import ConfigParser as configparser
try:
    import configparser
except ImportError:
    import ConfigParser as configparser


class lora_decode(gr.top_block):

    def __init__(self):
        gr.top_block.__init__(self, "LoRa Decode")

        ##################################################
        # Variables
        ##################################################
        self._spreading_factor_config = configparser.ConfigParser()
        self._spreading_factor_config.read('lora_decode.conf')
        try: spreading_factor = self._spreading_factor_config.getint('LoRa', 'spreading_factor')
        except: spreading_factor = 9
        self.spreading_factor = spreading_factor
        self._samp_rate_config = configparser.ConfigParser()
        self._samp_rate_config.read('lora_decode.conf')
        try: samp_rate = self._samp_rate_config.getfloat('Default', 'samp_rate')
        except: samp_rate = 1e6
        self.samp_rate = samp_rate
        self._implicit_header_config = configparser.ConfigParser()
        self._implicit_header_config.read('lora_decode.conf')
        try: implicit_header = self._implicit_header_config.getboolean('LoRa', 'implicit_header')
        except: implicit_header = False
        self.implicit_header = implicit_header
        self._crc_enable_config = configparser.ConfigParser()
        self._crc_enable_config.read('lora_decode.conf')
        try: crc_enable = self._crc_enable_config.getboolean('LoRa', 'crc_enable')
        except: crc_enable = True
        self.crc_enable = crc_enable
        self._coding_rate_config = configparser.ConfigParser()
        self._coding_rate_config.read('lora_decode.conf')
        try: coding_rate = self._coding_rate_config.getint('LoRa', 'coding_rate')
        except: coding_rate = 1
        self.coding_rate = coding_rate
        self._channel_freq_config = configparser.ConfigParser()
        self._channel_freq_config.read('lora_decode.conf')
        try: channel_freq = self._channel_freq_config.getfloat('LoRa', 'channel_freq')
        except: channel_freq = 869.525e6
        self.channel_freq = channel_freq
        self._center_freq_config = configparser.ConfigParser()
        self._center_freq_config.read('lora_decode.conf')
        try: center_freq = self._center_freq_config.getfloat('Default', 'center_freq')
        except: center_freq = 868.3e6
        self.center_freq = center_freq
        self._bandwidth_config = configparser.ConfigParser()
        self._bandwidth_config.read('lora_decode.conf')
        try: bandwidth = self._bandwidth_config.getfloat('LoRa', 'bandwidth')
        except: bandwidth = 125000
        self.bandwidth = bandwidth

        ##################################################
        # Blocks
        ##################################################
        self.zeromq_pub_msg_sink_0_0 = zeromq.pub_msg_sink('tcp://0.0.0.0:2002', 100, True)
        self.zeromq_pub_msg_sink_0 = zeromq.pub_msg_sink('tcp://0.0.0.0:2003', 100, True)
        self.osmosdr_source_0 = osmosdr.source(
            args="numchan=" + str(1) + " " + ""
        )
        self.osmosdr_source_0.set_time_unknown_pps(osmosdr.time_spec_t())
        self.osmosdr_source_0.set_sample_rate(samp_rate)
        self.osmosdr_source_0.set_center_freq(center_freq, 0)
        self.osmosdr_source_0.set_freq_corr(0, 0)
        self.osmosdr_source_0.set_dc_offset_mode(2, 0)
        self.osmosdr_source_0.set_iq_balance_mode(0, 0)
        self.osmosdr_source_0.set_gain_mode(False, 0)
        self.osmosdr_source_0.set_gain(10, 0)
        self.osmosdr_source_0.set_if_gain(20, 0)
        self.osmosdr_source_0.set_bb_gain(20, 0)
        self.osmosdr_source_0.set_antenna('', 0)
        self.osmosdr_source_0.set_bandwidth(0, 0)
        self.lora_lora_receiver_0_0 = lora.lora_receiver(samp_rate, center_freq, [channel_freq], bandwidth, spreading_factor, implicit_header, coding_rate, crc_enable, False, True, 1, False, False)
        self.lora_lora_receiver_0 = lora.lora_receiver(samp_rate, center_freq, [channel_freq], bandwidth, 8, True, 4, crc_enable, False, False, 1, False, False)



        ##################################################
        # Connections
        ##################################################
        self.msg_connect((self.lora_lora_receiver_0, 'frames'), (self.zeromq_pub_msg_sink_0_0, 'in'))
        self.msg_connect((self.lora_lora_receiver_0_0, 'frames'), (self.zeromq_pub_msg_sink_0, 'in'))
        self.connect((self.osmosdr_source_0, 0), (self.lora_lora_receiver_0, 0))
        self.connect((self.osmosdr_source_0, 0), (self.lora_lora_receiver_0_0, 0))


    def get_spreading_factor(self):
        return self.spreading_factor

    def set_spreading_factor(self, spreading_factor):
        self.spreading_factor = spreading_factor
        self.lora_lora_receiver_0_0.set_sf(self.spreading_factor)

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.osmosdr_source_0.set_sample_rate(self.samp_rate)

    def get_implicit_header(self):
        return self.implicit_header

    def set_implicit_header(self, implicit_header):
        self.implicit_header = implicit_header

    def get_crc_enable(self):
        return self.crc_enable

    def set_crc_enable(self, crc_enable):
        self.crc_enable = crc_enable

    def get_coding_rate(self):
        return self.coding_rate

    def set_coding_rate(self, coding_rate):
        self.coding_rate = coding_rate

    def get_channel_freq(self):
        return self.channel_freq

    def set_channel_freq(self, channel_freq):
        self.channel_freq = channel_freq

    def get_center_freq(self):
        return self.center_freq

    def set_center_freq(self, center_freq):
        self.center_freq = center_freq
        self.lora_lora_receiver_0.set_center_freq(self.center_freq)
        self.lora_lora_receiver_0_0.set_center_freq(self.center_freq)
        self.osmosdr_source_0.set_center_freq(self.center_freq, 0)

    def get_bandwidth(self):
        return self.bandwidth

    def set_bandwidth(self, bandwidth):
        self.bandwidth = bandwidth





def main(top_block_cls=lora_decode, options=None):
    tb = top_block_cls()

    def sig_handler(sig=None, frame=None):
        tb.stop()
        tb.wait()

        sys.exit(0)

    signal.signal(signal.SIGINT, sig_handler)
    signal.signal(signal.SIGTERM, sig_handler)

    tb.start()

    try:
        input('Press Enter to quit: ')
    except EOFError:
        pass
    tb.stop()
    tb.wait()


if __name__ == '__main__':
    main()
