import zmq
import time

ctx = zmq.Context()
sock = ctx.socket(zmq.SUB)
sock.connect("tcp://192.168.1.185:2002")
sock.subscribe("") # Subscribe to all topics

print("Starting receiver loop ...")
while True:
    msg = sock.recv()
    for x in msg:
        print(f'{x:02x} ', end='')
    print()

sock.close()
ctx.term()